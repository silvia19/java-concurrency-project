import java.util.Timer;
import java.util.TimerTask;

public class ElfThread extends Thread {
	
	public ElfThread() {
		
	}

	public void run() {
		System.out.println("An elf was created!");
		
		// Created every 500-1000 milliseconds
		Timer timer = new Timer();
		timer.schedule (completeTask(), 500, 1000);
	}
	
	public TimerTask completeTask() {
		System.out.println("Task completed.");
		return null;
	}
}

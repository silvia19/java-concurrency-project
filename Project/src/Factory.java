import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Factory {

	// Random generator for the dimension of the factory
	static Random rand = new Random();
	public static int N = rand.nextInt(500) + 100;
	
	public static int[][] position = new int[N][N];
	public static Map<Integer, Boolean> factoryMap = new HashMap<Integer, Boolean>();
	// Boolean constant to test if the position is occupied or not
	public static boolean free = true; 

	// Method for finding the elf's position in the factory
	public synchronized void findPosition() {
		// Matrix layout factory
		System.out.println("The place in the factory is: ");
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				System.out.print(position[i][j]);
			}
		}
	}

	// Method for creating the toys
	public static synchronized void createToys() {
		System.out.println("A toy was created!");
	}

	// Method for creating elves
	public synchronized void createElf() {

		int i, j;

		// Random generator for the number of elves working in the factory
		Random rand = new Random();
		int elvesNumber = rand.nextInt(N / 2) + 1;

		while (elvesNumber != 0) {
			ElfThread elfThread = new ElfThread();

			// Generating a random position for the elf
			i = rand.nextInt(N - 1) + 0;
			j = rand.nextInt(N - 1) + 0;
			position[i][j] = rand.nextInt(N - 1) + 0;

			(new ElfThread()).start();

			// Associating an 'occupied' boolean tag for each position in the factory
			factoryMap.put(Factory.position[i][j], free);
//			free = false; // Associate every place where an elf appeared with 'occupied'

			// Determining if all the positions in the factory have been used by elves
			int testBool = 0; // Constant for determining if all the positions have been occupied
			for (int row = 0; row <= N - 1; row++) {
				for (int column = 0; column <= N - 1; column++) {
					if (free == false) // Test if the positions were occupied
						testBool = 1;
					else {
						testBool = 0;
						break;
					}
				}
			}

			while (testBool == 0) {
				// Condition for moving the elf; else, it stays for 10 milliseconds
				if (Elf.elfMove == true)
					Elf.move();
				else {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Elf.move();
				}
				elvesNumber--;
			}
		}
	}
}

import java.util.concurrent.Semaphore;

public class Elf {

	public static int N = 500;  // (am initializat N in clasa Factory ca public insa tot nu il vede)
	int noOfToys;
	public static Boolean elfMove;

	// Method for moving around the factory
	public static synchronized void move() {
		int i = 0, j = 0;

		// Verification conditions to test if there is a space in every direction and if it's free
		if (i - 1 >= 0 && i - 1 < N && Factory.free == true) // Up
			Factory.position[i][j] = Factory.position[i - 1][j];

		if (i + 1 >= 0 && i + 1 < N && Factory.free == true) // Down
			Factory.position[i][j] = Factory.position[i + 1][j];

		if (j - 1 >= 0 && j - 1 < N && Factory.free == true) // Left
			Factory.position[i][j] = Factory.position[i][j - 1];

		if (j + 1 >= 0 && j + 1 < N && Factory.free == true) // Right
			Factory.position[i][j] = Factory.position[i][j + 1];

		// After every move they create a toy
		Factory.createToys();
	}

	// Method for whole work of elves
	@SuppressWarnings("null")
	public synchronized void run() {
		// Create toys every 30 milliseconds
		Factory.createToys();
		try {
			wait(30);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// At each move, the elf creates a toy; after that it takes a break of 30 milliseconds
		if (elfMove == true) {
			move();
			Factory.createToys();
			try {
				wait(30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public synchronized void retreat() {
		// A new thread that retreats the elves
		ElfThread retreatThread = new ElfThread();
		
		// Semaphore for the elves that need to be retreated
		Semaphore semaphore = new Semaphore(0);
		
		// Getting the elves that will be retreated
		try {
	        if(semaphore.tryAcquire()) {
	            System.out.println(Thread.currentThread().getName() + " aquired for 3 seconds " + semaphore.toString());
	            Thread.sleep(3000); 
	        } else {
	            System.out.println(Thread.currentThread().getName() + " left due to full shop");
	        }

	    }
	    catch (InterruptedException e) {
	       e.printStackTrace();
	    } finally {   
	       semaphore.release();
	       System.out.println(Thread.currentThread().getName() + " released " + semaphore.toString());
	    }
		
	}
}

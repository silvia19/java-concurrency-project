import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CyclicBarrier {
	public CyclicBarrier(int parties) {
	}

	public CyclicBarrier(int parties, Runnable barrierAction) {
	}

	public int await() throws InterruptedException, BrokenBarrierException {
		return 0;
	}

	public int await(long timeout, TimeUnit unit) throws InterruptedException, BrokenBarrierException, TimeoutException {
		return 0;
	}

	public void reset() {
	}

	public boolean isBroken() {
		return false;
	}

	public int getParties() {
		return 0;
	}

	public int getNumberWaiting() {
		return 0;
	}
}
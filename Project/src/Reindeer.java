import java.util.Random;
import java.util.concurrent.Semaphore;

public class Reindeer implements Runnable {

	int id;
	CyclicBarrier cyclicBarrier;

	// We identify each reindeer by its ID number
	public Reindeer(int id) {
		this.id = id;
	}

	// Method for packing the toys
	public synchronized void packToys() {
		System.out.println("A toy was packed!");
	}

	// Method for whole work of reindeer
	public synchronized void run() {
		// There are between 8 and 10 reindeer working at once
		Random rand = new Random();
		int reindeerNumber = rand.nextInt(10) + 8;

		// Barrier for reindeer because more wait to work at the same time
//		public final CyclicBarrier allReindeer;
//		allReindeer = new CyclicBarrier(reindeerNumber, new Runnable() {
//			public void run() {
//				System.out.println(reindeerNumber + " reindeer started working.");
//			}
//		});

		while (reindeerNumber != 0) {
			// Every reindeer is represented by a thread
			ReindeerThread reindeerThread = new ReindeerThread();

			Semaphore elves = new Semaphore(1);
			Semaphore reindeer = new Semaphore(1);

			// Taking the toys from the elves using semaphores]
			while (true) {
				// Semaphore for elves
				try {
					elves.acquire();
				} catch (InterruptedException e) {
				} finally {
					elves.release();
				}

				// Semaphore for reindeer
				try {
					reindeer.acquire();
				} catch (InterruptedException e) {
				} finally {
					reindeer.release();
				} 
			}
//			reindeerNumber--;
		}
	}
}
